const NDEF = require("ndef");
const { NFC } = require("nfc-pcsc");
const debug = require('debug')('debug')
const { Mutex } = require('async-mutex')

const NOT_FOUND = -1;
const PCSCAID = Buffer.from('A000000306', 'hex');
const ndefAid = Buffer.from("D2760000850101", "hex");
const ndefFileId = Buffer.from("e104", "hex");
const success = Buffer.from("9000", "hex");
const nfc = new NFC();
const pageSize = 4;
const mutex = new Mutex();

function wrap(CLA = 0x00, ins, p1 = 0, p2 = 0, dataIn) {
  //TODO: support string, buffer, array for dataIn

  const length = dataIn.length;
  const buf = Buffer.from([CLA, ins, p1, p2, length, ...dataIn]);
  return [CLA, ins, p1, p2, length, ...dataIn, 0x00];
}

nfc.on("reader", async reader => {
  reader.autoProcessing = false;
  const { name } = reader;
  if (name === 'Yubico YubiKey OTP+FIDO+CCID') {
    return;
  }
  // just handy shortcut to send data
  const send = async (cmd, comment = null, responseMaxLength = 40) => {
    const b =
      typeof cmd === "string" ? Buffer.from(cmd, "hex") : Buffer.from(cmd);
    debug((comment ? `[${comment}] ` : "") + `sending`, b);
    const data = await reader.transmit(b, responseMaxLength);
    debug((comment ? `[${comment}] ` : "") + `received data`, data);

    return data;
  };

  async function read(page, length = 4) {
    // With my uTrust 3700, it returns 4 bytes per packet, not the 16 the library expects
    return await reader.read(page, length, blockSize = 4, packetSize = 4);
  }

  async function type4ReadNdef() {
    let res;
    res = await send(wrap(0x00, 0xa4, 0x04, 0x0c, ndefAid), "select app");
    if (res.slice(-1)[0] !== 0x00) {
      throw new Error(`error in select ndef app ${res.toString('hex')}`);
    }

    res = await send(
      wrap(0x00, 0xa4, 0x00, 0x0c, ndefFileId),
      "select file"
    );

    if (res.slice(-1)[0] !== 0x00) {
      throw new Error(`error in select file ${res.toString('hex')}`);
    }

    res = await send([0x00, 0xb0, 0x00, 0x00, 0x80], "read ndef", 0x80 + 2);
    if (!success.equals(res.slice(-2))) {
      throw new Error(`error in read ndef ${res.toString('hex')}`);
    }

    const length = res.readUInt16BE();
    return res.slice(2, 2 + length);
  };

  async function type2ReadNdef() {
    const CC = await read(3);
    if (CC[0] != 0xe1) {
      throw new Error("Magic byte for NDEF missing");
    }

    const size = CC[2];
    const startingPage = 4;
    let lastUserPage = startingPage;
    switch (size) {
      case 0x12:
        lastUserPage = 0x27;
        break;
      case 0x3e:
        lastUserPage = 0x81;
        break;
      case 0x6d:
        lastUserPage = 0xe1;
        break;
    }

    const TLV = {
      NULL: 0, LockControl: 1, MemoryControl: 2, NDEFMessage: 3, Terminator: 0xfe
    }
    let pageNum = startingPage;
    let index = 0; // The offset within the page
    let ndefFound = false;
    do {
      const page = await read(pageNum);
      switch (page[index++]) {
        case TLV.LockControl:
          index += 1 + page[index];
          break;
        case TLV.NDEFMessage:
          ndefFound = true;
          break;
        default:
          console.log("Buffer didn't start with handled NDEF TLV");
          return Buffer.alloc(0);
      }
      pageNum += Math.floor(index / pageSize);
      index = index % pageSize;
    } while (!ndefFound && pageNum < lastUserPage);

    const page = await read(pageNum);
    const length = page[index];
    if (length === 0xff) {
      throw new Error("No support for ndef > 255 yet");
    }
    const byteCount = Math.ceil(length / pageSize) * pageSize;

    let buffer = Buffer.alloc(0);
    buffer = await read(pageNum, byteCount)
    return buffer.slice(index+1, index + 1 + length);
  }

  function parseNdef(buffer) {
    const records = NDEF.decodeMessage(buffer);
    return records
      .map(record => {
        switch (record.tnf) {
          case NDEF.TNF_WELL_KNOWN:
            switch (record.type) {
              case NDEF.RTD_URI:
                return NDEF.uri.decodePayload(record.payload);
              default:
                console.log("other type", record.type);
            }
          default:
            console.log("other tnf", record.tnf);
        }
        return null;
      })
      .filter(x => x)
      .join('\n');
  }

  reader.on("card", async card => {
    const release = await mutex.acquire();
    try {
      const { atr, type } = card;
      if (type === 'TAG_ISO_14443_4') {
        const ndef = await type4ReadNdef();
        console.log(parseNdef(ndef));
      } else if (type === 'TAG_ISO_14443_3') {
        const ndef = await type2ReadNdef();
        console.log(parseNdef(ndef));
      }
      if (!process.stdout.isTTY) {
        process.exit(0);
      }
    } catch (e) {
      console.log("card error", e);
    } finally {
      release();
    }
  });

  reader.on('card.off', card => {
    console.log(`${reader.reader.name}  card removed`, card);
  });

  reader.on("error", err => {
    console.error(`an error occurred`, reader, err);
  });

  reader.on("end", () => {
    console.info(`device removed`, reader);
  });
});

nfc.on("error", err => {
  console.error(`an error occurred`, err);
});
