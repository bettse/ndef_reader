# NDEF Reader

Assuming a PC/SC NFC reader/writer.  I use a uTrust 3700, but an ACR122U should work as well.

Will attempt to read NDEF record off of type 2 (NTAG21x) or type 4 (NTAG424/DesFire/etc), and it it is a URL, decode and print it.

`node index.js`
